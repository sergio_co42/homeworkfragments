package com.academiasmoviles.homeworkframents.productos

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.academiasmoviles.homeworkframents.R
import com.academiasmoviles.homeworkframents.contactos.ContactosListener
import com.academiasmoviles.homeworkframents.modelo.ContactoEntity
import com.academiasmoviles.homeworkframents.modelo.ProductoEntity
import kotlinx.android.synthetic.main.fragment_contactos_detalle.*
import kotlinx.android.synthetic.main.fragment_productos_detalle.*

class ProductosDetalleFragment : Fragment() {
    private var listener: ProductosListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_productos_detalle, container, false)
    }


    fun renderProduct(productoEntity: ProductoEntity) {
        val name = productoEntity.name
        val price = productoEntity.precio
        val description = productoEntity.descripcion


        tvNombre.text = name
        tvPrecio.text = price
        tvDescripcion.text = description
        ivProducto.setImageResource(productoEntity.photo)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProductosListener) {
            listener = context
        } else {
            throw RuntimeException("$context ProductosListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}