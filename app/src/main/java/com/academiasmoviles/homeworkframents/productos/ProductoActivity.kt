package com.academiasmoviles.homeworkframents.productos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.FragmentManager
import com.academiasmoviles.homeworkframents.R
import com.academiasmoviles.homeworkframents.modelo.ProductoEntity

class ProductoActivity : AppCompatActivity() , ProductosListener {

    private lateinit var productsFragment: ProductosFragment
    private lateinit var productosDetailFragment: ProductosDetalleFragment
    private lateinit var fragmentManager: FragmentManager


    override fun selectedItemProduct(productoEntity: ProductoEntity) {
        Log.v("CONSOLE", "selectedItemProduct")
        productosDetailFragment.renderProduct(productoEntity)
    }

    override fun renderFirst(productoEntity: ProductoEntity?) {
        productoEntity?.let {
            selectedItemProduct(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_producto)

        fragmentManager = supportFragmentManager
        productsFragment = fragmentManager.findFragmentById(R.id.fragmentProductos) as ProductosFragment
        productosDetailFragment = fragmentManager.findFragmentById(R.id.fragmentProductosDetalle) as ProductosDetalleFragment
    }

}