package com.academiasmoviles.homeworkframents.productos

import com.academiasmoviles.homeworkframents.modelo.ProductoEntity

interface ProductosListener {
    fun selectedItemProduct(productoEntity: ProductoEntity)
    fun renderFirst(productoEntity: ProductoEntity?)
}