package com.academiasmoviles.homeworkframents.productos

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.academiasmoviles.homeworkframents.R
import com.academiasmoviles.homeworkframents.adapter.ContactoAdapter
import com.academiasmoviles.homeworkframents.adapter.ProductoAdapter
import com.academiasmoviles.homeworkframents.contactos.ContactosListener
import com.academiasmoviles.homeworkframents.modelo.ContactoEntity
import com.academiasmoviles.homeworkframents.modelo.ProductoEntity
import kotlinx.android.synthetic.main.fragment_contactos.*
import kotlinx.android.synthetic.main.fragment_productos.*

class ProductosFragment : Fragment() {

    private var listener: ProductosListener? = null

    private var productos = mutableListOf<ProductoEntity>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_productos, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProductosListener) {
            listener = context
        } else {
            throw RuntimeException("$context ProductosListener")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setData()

        listaProductos.adapter = ProductoAdapter(requireContext(), productos)

        listaProductos.setOnItemClickListener { _, _, i, _ ->
            listener?.let {
                it.selectedItemProduct(productos[i])
            }
        }

        listener?.renderFirst(first())

    }

    private fun setData() {
        val product1 = ProductoEntity(1, "Maca", "S/.20", "La maca (Lepidium meyenii) es una planta que crece sobre los 4000 metros de altitud en los Andes Centrales del Perú, presenta diferentes variedades de acuerdo al color de su hipocótilo. La presente revisión resume los resultados de estudios sobre los efectos de la maca en la función sexual, la espermatogénesis, la función reproductiva femenina, la memoria, la depresión y la ansiedad, como energizante y contra la hiperplasia benigna de próstata, osteoporosis y síndrome metabólico. Se discute también su efecto antienvejecimiento y la seguridad en su consumo.",
            R.drawable.maca)

        val product2 = ProductoEntity(2, "Quinua", "S/.15", "Es una planta herbácea anual, que normalmente alcanza una altura de 1 a 3 m. Las hojas, alternas, son anchas y polimorfas; el tallo central puede estar más o menos ramificado, dependiendo de la variedad o densidad del sembrado. Las flores, organizadas en panículas, son pequeñas y carecen de pétalos. Las terminales son hermafroditas o masculinas y las laterales generalmente femeninas. El fruto es un utrículo (aquenio de pericarpo membranoso) de unos 2 mm de diámetro; tiene semillas lenticulares con abundante perisperma harinoso.",
            R.drawable.quinua)

        val product3 = ProductoEntity(3, "Lúcuma", "S/.5", "El lúcumo (Pouteria lucuma) es la especie más extendida y comercialmente más valiosa del género Pouteria. Es un árbol de la familia de las sapotáceas, originaria y nativa de los valles andinos del Ecuador, Perú y Chile que se cultiva por su fruto llamado lúcuma empleado en gastronomía, especialmente en la peruana, sobre todo en la preparación de dulces, postres y helados.",
            R.drawable.lucuma)

        val product4 = ProductoEntity(4, "Chia", "S/.25", "Salvia hispanica, de nombre común chía es una herbácea de la familia de las lamiáceas; es nativa del centro y sur de México, El Salvador, Guatemala, Nicaragua1\u200B y Costa Rica y, junto con el lino (Linum usitatissimum), es una de las especies vegetales con la mayor concentración de ácido graso alfa-linolénico omega 3 conocidas hasta 2006.",
            R.drawable.chia)

        productos.add(product1)
        productos.add(product2)
        productos.add(product3)
        productos.add(product4)
    }


    private fun first(): ProductoEntity? = productos?.first()

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}