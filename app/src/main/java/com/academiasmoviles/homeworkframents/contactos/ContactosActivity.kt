package com.academiasmoviles.homeworkframents.contactos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.FragmentManager
import com.academiasmoviles.homeworkframents.R
import com.academiasmoviles.homeworkframents.modelo.ContactoEntity

class ContactosActivity : AppCompatActivity(), ContactosListener {

    private lateinit var contactsFragment: ContactosFragment
    private lateinit var contactDetailFragment: ContactosDetalleFragment
    private lateinit var fragmentManager: FragmentManager


    override fun selectedItemContact(contactEntity: ContactoEntity) {
        Log.v("CONSOLE", "selectedItemContact")
        contactDetailFragment.renderContact(contactEntity)
    }

    override fun renderFirst(contactEntity: ContactoEntity?) {
        contactEntity?.let {
            selectedItemContact(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contactos)

        fragmentManager = supportFragmentManager
        contactsFragment = fragmentManager.findFragmentById(R.id.fragmentContactos) as ContactosFragment
        contactDetailFragment = fragmentManager.findFragmentById(R.id.fragmentContactosDetalle) as ContactosDetalleFragment
    }
}