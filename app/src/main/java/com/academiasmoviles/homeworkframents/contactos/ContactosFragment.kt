package com.academiasmoviles.homeworkframents.contactos

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.academiasmoviles.homeworkframents.R
import com.academiasmoviles.homeworkframents.adapter.ContactoAdapter
import com.academiasmoviles.homeworkframents.modelo.ContactoEntity
import kotlinx.android.synthetic.main.fragment_contactos.*


class ContactosFragment : Fragment() {
    private var listener: ContactosListener? = null

    private var contactos = mutableListOf<ContactoEntity>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contactos, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ContactosListener) {
            listener = context
        } else {
            throw RuntimeException("$context ContactosListener")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setData()

        listaContactos.adapter = ContactoAdapter(requireContext(), contactos)

        listaContactos.setOnItemClickListener { _, _, i, _ ->
            listener?.let {
                it.selectedItemContact(contactos[i])
            }
        }

        listener?.renderFirst(first())

    }

    private fun setData() {
        val contact1 = ContactoEntity(1, "Matias Tobar", "999852489", "mtobar@gmail.com",
            R.drawable.perfil_1, "mtobarDev.com")

        val contact2 = ContactoEntity(2, "Paola Sifuentes", "968254133", "psifuentes@gmail.com",
            R.drawable.perfil_2, "pSifDesign.com")

        val contact3 = ContactoEntity(3, "Andrea Jimenez", "958421169", "ajimenez@gmail.com",
            R.drawable.perfil_3, "Ajimenez.com")

        val contact4 = ContactoEntity(4, "Jimena del Pilar", "985744369", "jimenadelp@gmail.com",
            R.drawable.perfil_4, "DelPilar.com")

        contactos.add(contact1)
        contactos.add(contact2)
        contactos.add(contact3)
        contactos.add(contact4)
    }


    private fun first(): ContactoEntity? = contactos?.first()

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}