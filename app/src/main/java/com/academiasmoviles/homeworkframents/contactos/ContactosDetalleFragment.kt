package com.academiasmoviles.homeworkframents.contactos

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.academiasmoviles.homeworkframents.R
import com.academiasmoviles.homeworkframents.modelo.ContactoEntity
import kotlinx.android.synthetic.main.fragment_contactos_detalle.*

class ContactosDetalleFragment : Fragment() {
    private var listener: ContactosListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contactos_detalle, container, false)
    }


    fun renderContact(contactoEntity: ContactoEntity) {
        val name = contactoEntity.name
        val phone = contactoEntity.phone
        val email = contactoEntity.email
        val web = contactoEntity.website

        tvName.text = name
        tvPhone.text = phone
        tvEmail.text = email
        tvWebsite.text = web
        ivContacto.setImageResource(contactoEntity.photo)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ContactosListener) {
            listener = context
        } else {
            throw RuntimeException("$context ContactosListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}