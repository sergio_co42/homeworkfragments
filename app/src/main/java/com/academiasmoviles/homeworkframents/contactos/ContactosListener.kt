package com.academiasmoviles.homeworkframents.contactos

import com.academiasmoviles.homeworkframents.modelo.ContactoEntity

interface ContactosListener {
    fun selectedItemContact(contactoEntity: ContactoEntity)
    fun renderFirst(contactoEntity: ContactoEntity?)
}