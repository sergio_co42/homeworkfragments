package com.academiasmoviles.homeworkframents.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.academiasmoviles.homeworkframents.R
import com.academiasmoviles.homeworkframents.modelo.ContactoEntity

class ContactoAdapter(private val context: Context, private val contactos:List<ContactoEntity>) :BaseAdapter() {
    override fun getCount(): Int= contactos.size

    override fun getItem(position: Int)= contactos[position]

    override fun getItemId(p0: Int):Long =0

    override fun getView(position: Int, view: View?, parent: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val container = inflater.inflate(R.layout.horizontalcontact, null)
        val imgContacto = container.findViewById<ImageView>(R.id.ivlContacto)
        val tvName = container.findViewById<TextView>(R.id.tvlName)

        //Extraer la entidad
        val contactoEntity = contactos[position]

        //Asociar la entidad con el XML
        tvName.text=contactoEntity.name
        imgContacto.setImageResource(contactoEntity.photo)

        return container
    }
}