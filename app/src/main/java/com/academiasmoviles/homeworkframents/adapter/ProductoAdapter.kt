package com.academiasmoviles.homeworkframents.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.academiasmoviles.homeworkframents.R
import com.academiasmoviles.homeworkframents.modelo.ProductoEntity


class ProductoAdapter(private val context: Context, private val productos:List<ProductoEntity>) :
    BaseAdapter() {
    override fun getCount(): Int= productos.size

    override fun getItem(position: Int)= productos[position]

    override fun getItemId(p0: Int):Long =0

    override fun getView(position: Int, view: View?, parent: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val container = inflater.inflate(R.layout.horizontalproducto, null)
        val imgProducto = container.findViewById<ImageView>(R.id.ivlProducto)
        val tvName = container.findViewById<TextView>(R.id.tvlName)
        val tvPrecio= container.findViewById<TextView>(R.id.tvlPrecio)

        //Extraer la entidad
        val productoEntity = productos[position]

        //Asociar la entidad con el XML
        tvName.text=productoEntity.name
        tvPrecio.text= productoEntity.precio
        imgProducto.setImageResource(productoEntity.photo)

        return container
    }
}