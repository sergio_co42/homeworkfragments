package com.academiasmoviles.homeworkframents.modelo

import java.io.Serializable

data class ContactoEntity (val id: Int, val name: String, val phone: String,
                           val email: String, val photo: Int, val website: String) : Serializable