package com.academiasmoviles.homeworkframents.modelo

import java.io.Serializable

data class ProductoEntity (val id: Int, val name: String, val precio: String,
                           val descripcion: String, val photo: Int) : Serializable